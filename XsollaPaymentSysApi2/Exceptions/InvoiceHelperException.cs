﻿using System;

namespace XsollaPaymentSysApi2.Exceptions
{
    public class InvoiceHelperException : Exception
    {

        public InvoiceHelperException(string message):base(message)
        {
        }
    }
}
