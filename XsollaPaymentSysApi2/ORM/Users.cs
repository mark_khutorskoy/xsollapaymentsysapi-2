﻿using System;
using System.Collections.Generic;

namespace XsollaPaymentSysApi2
{
    public partial class Users
    {
        public Users()
        {
            InvoiceInfo = new HashSet<InvoiceInfo>();
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public int? SessionInfoId { get; set; }

        public virtual SessionInfo SessionInfo { get; set; }
        public ICollection<InvoiceInfo> InvoiceInfo { get; set; }
    }
}
