﻿using System;
using System.Collections.Generic;

namespace XsollaPaymentSysApi2
{
    public partial class SessionInfo
    {
        public SessionInfo()
        {
            Users = new HashSet<Users>();
        }

        public int Id { get; set; }
        public string SessionId { get; set; }
        public string SessionExpiration { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}
