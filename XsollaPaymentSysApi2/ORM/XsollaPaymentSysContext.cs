﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace XsollaPaymentSysApi2
{
    public partial class XsollaPaymentSysContext : DbContext
    {
        public XsollaPaymentSysContext()
        {
        }

        public XsollaPaymentSysContext(DbContextOptions<XsollaPaymentSysContext> options)
            : base(options)
        {
        }

        public virtual DbSet<InvoiceInfo> InvoiceInfo { get; set; }
        public virtual DbSet<SessionInfo> SessionInfo { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-DUJP9DP;Database=XsollaPaymentSys;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InvoiceInfo>(entity =>
            {
                entity.ToTable("invoiceInfo");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.PaymentAmount)
                    .HasColumnName("paymentAmount")
                    .HasColumnType("decimal(18, 0)");

                entity.Property(e => e.PaymentDate)
                    .HasColumnName("paymentDate")
                    .HasColumnType("date");

                entity.Property(e => e.PaymentPurpose)
                    .IsRequired()
                    .HasColumnName("paymentPurpose");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnName("status")
                    .HasMaxLength(20);

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.InvoiceInfo)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_invoiceInfo_invoiceInfo");
            });

            modelBuilder.Entity<SessionInfo>(entity =>
            {
                entity.ToTable("sessionInfo");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.SessionExpiration)
                    .HasColumnName("sessionExpiration")
                    .HasMaxLength(50);

                entity.Property(e => e.SessionId)
                    .IsRequired()
                    .HasColumnName("sessionId");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("users");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasColumnName("login");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasColumnName("password");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasColumnName("salt");

                entity.Property(e => e.SessionInfoId).HasColumnName("sessionInfoId");

                entity.HasOne(d => d.SessionInfo)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.SessionInfoId)
                    .HasConstraintName("FK_users_sessionInfo");
            });
        }
    }
}
