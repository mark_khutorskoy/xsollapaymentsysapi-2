﻿using System;
using System.Collections.Generic;

namespace XsollaPaymentSysApi2
{
    public partial class InvoiceInfo
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public decimal PaymentAmount { get; set; }
        public string PaymentPurpose { get; set; }
        public string Status { get; set; }
        public DateTime? PaymentDate { get; set; }

        public Users User { get; set; }
    }
}
