﻿namespace XsollaPaymentSysApi2
{
    public class CardDate
    {
        public int Month { get; set; }
        public int Year { get; set; }

        public CardDate(int month, int year)
        {
            Month = month;
            Year = year;
        }
    }
}
