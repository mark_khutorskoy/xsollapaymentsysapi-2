﻿namespace XsollaPaymentSysApi2
{
    public class CardInfo
    {
        public string CardKey { set; get; }
        public string CardVerificationCode { set; get; }
        public CardDate Date { set; get; }

        public CardInfo(string cardkey, string cvc, CardDate date)
        {
            CardKey = cardkey;
            CardVerificationCode = cvc;
            Date = date;
        }
    }
}
