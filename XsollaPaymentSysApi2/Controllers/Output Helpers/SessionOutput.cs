﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace XsollaPaymentSysApi2.Controllers.Output_Helpers
{
    public class SessionOutput: HttpStatusCodeResult
    {
        public object InvoiceRowId { get; set; }

        public SessionOutput(HttpStatusCode code, string message = null, int invoiceRowId = -1):base(code, message)
        {
            InvoiceRowId = invoiceRowId;
        }
    }
}
