﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using XsollaPaymentSysApi2.Controllers.Output_Helpers;
using XsollaPaymentSysApi2.Invoice;
using System.Web.Mvc;
using XsollaPaymentSysApi2.Exceptions;
using XsollaPaymentSysApi2.UsersOp;
using ContentResult = Microsoft.AspNetCore.Mvc.ContentResult;
using ControllerBase = Microsoft.AspNetCore.Mvc.ControllerBase;

namespace XsollaPaymentSysApi2.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    [ApiController]
    public class PaymentSystemController : ControllerBase
    {
        private readonly XsollaPaymentSysContext _context;

        public PaymentSystemController(XsollaPaymentSysContext context)
        {
            _context = context;
        }

        // GET api/PaymentSystem
        [Microsoft.AspNetCore.Mvc.HttpGet]
        public HttpStatusCodeResult Get()
        {
            return new HttpStatusCodeResult(HttpStatusCode.OK, "Я работаю :)");
        }

        /// <summary>
        /// Собирает начальные данные платежа перед началом сессии
        /// </summary>
        /// <param name="paymentAmount">Размер платежа</param>
        /// <param name="paymentPurpose">Назначение платежа</param>
        /// <returns>sessionId</returns>
        [Microsoft.AspNetCore.Mvc.HttpPost("session")]
        public HttpStatusCodeResult GetSessionId(decimal paymentAmount, string paymentPurpose)
        {
            if (paymentAmount == 0 || paymentPurpose == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Argument null error!");

            SessionInfo sessionInfo = new Authentication().CreateSessionObj();
            var sessionRowId = new InvoiceHelper().SessionAndUnpaidInvoiceSave(paymentAmount,paymentPurpose,sessionInfo);

            return new SessionOutput(HttpStatusCode.OK, sessionInfo.SessionId, sessionRowId);
        }

        /// <summary>
        /// Имитирует оплату
        /// </summary>
        /// <param name="cardKey">Номер карты</param>
        /// <param name="cvc">код карты</param>
        /// <param name="date">даты карты</param>
        /// <param name="sessionId"></param>
        /// <param name="invoiceRowId">Id строки созданной в методе PaymentInfo для обновления инвойса </param>
        /// <returns>результат операции оплаты (успех/нет)</returns>
        [Microsoft.AspNetCore.Mvc.HttpPut("pay")]
        public HttpStatusCodeResult Pay(string cardKey, string cvc, string date, string sessionId, int invoiceRowId)
        {
            if (cardKey == null || cvc.Length != 3 || date == null || sessionId == null || invoiceRowId <0)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Argument null error!");

            var sesChRes= new Authentication().SessionCheckSuccess(sessionId);
            if (!sesChRes.Successful)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized, sesChRes.Message);

            var isCardValid = (new LuhnSimplifiedAlgorithm().CardKeyValidation(cardKey));
            if (!isCardValid)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden, "Неверный номер карты ");
      
            var partialDate = date.Split('/');
            var card = new CardInfo(cardKey, cvc, new CardDate(Convert.ToInt32(partialDate[0]), Convert.ToInt32(partialDate[1])));

            try
            {
                new InvoiceHelper().PaidInvoiceSave(invoiceRowId);
            }
            catch (InvoiceHelperException exception)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, exception.Message);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK, "Оплата прошла успешно");
        }

        /// <summary>
        /// Вовзращает список платежей за переданный период (закрыт авторизацией)
        /// </summary>
        /// <param name="startDate">Начала периода (включительно)</param>
        /// <param name="endDate">Конец периода (включительно)</param>
        /// <param name="login"></param>
        /// <param name="pass"></param>
        [Microsoft.AspNetCore.Mvc.HttpGet("paymentsList")]
        public ContentResult GetPaymentsList(DateTime startDate, DateTime endDate, string login, string pass)
        {
            var authRes = new Authentication().LogIn(login, pass);

            ContentResult res;
            if (!authRes.Successful)
            {
                res = Content(authRes.Message);
                res.StatusCode = 401;
                return res;
            }

            var invlist = new InvoiceHelper().GetInvoicesList(startDate,endDate);
            
            res = Content(JsonConvert.SerializeObject(invlist), "application/json");
            res.StatusCode = 200;
            return res;
        }

        [Microsoft.AspNetCore.Mvc.HttpPost("signUp")]
        public HttpStatusCodeResult RegUser(string login, string pass)
        {
            if (login==null || pass == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Argument null error!");

            if (!new Authentication().CheckLoginUniqueness(login))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Пользователь с таким логином уже существует, придумайте другой");

            new Registration().Registr(login,pass);

            return new HttpStatusCodeResult(HttpStatusCode.OK, "Регистрация успешна");
        }

        [Microsoft.AspNetCore.Mvc.HttpPost("signIn")]//потому что создается сессия в бд
        public HttpStatusCodeResult LogInUser(string login, string pass)
        {
            if (login==null || pass==null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Argument null error!");

            var auth = new Authentication();

            if (auth.IsAuthorized(login, pass).Successful)
                return new HttpStatusCodeResult(HttpStatusCode.OK, "Вы уже авторизованы");

            var result = auth.LogIn(login, pass);
            if (!result.Successful)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized, result.Message);

            return new HttpStatusCodeResult(HttpStatusCode.OK, "Авторизация успешна");   
        }
    }
}

