﻿namespace XsollaPaymentSysApi2
{
    /// <summary>
    /// Упрощенный алгоритм Луна для проверки валидности номера карты
    /// </summary>
    public class LuhnSimplifiedAlgorithm
    {
        public bool CardKeyValidation(string cardKey)
        {
            var cardKeyArray = CreateWorkingArray(cardKey);
            cardKeyArray = DoStepOne(cardKeyArray);
            bool result = DoStepTwo(cardKeyArray);

            return result;
        }

        /// <summary>
        /// Создает "рабочий" массив для алгоритма Луна
        /// </summary>
        /// <param name="cardKey">Номер карты</param>
        /// <returns>Неизмененный номер карты в массиве интов</returns>
        public int[] CreateWorkingArray(string cardKey)
        {
            var cardKeyArray = new int[cardKey.Length];

            for (int i = 0; i < cardKey.Length; i++)
                cardKeyArray[i] = cardKey[i] - '0';

            return cardKeyArray;
        }

        /// <summary>
        /// Манипуляции с каждой второй цифрой карты (удвоение и выситание по условию)
        /// </summary>
        /// <param name="cardKeyArray">Рабочий массив цифр карты</param>
        /// <returns>Изменный массив цифр карты согласно первому шагу алгоритма</returns>
        public int[] DoStepOne(int[] cardKeyArray)
        {
            var startPos = 0;
            if (cardKeyArray.Length % 2 != 0) startPos = 1;

            for (int i = startPos; i < cardKeyArray.Length; i = i + 2)
            {
                var curValue = cardKeyArray[i] * 2;
                if (curValue > 9) curValue = curValue - 9;
                cardKeyArray[i] = curValue;
            }

            return cardKeyArray;
        }

        /// <summary>
        /// Шаг 2 - Суммирование цифр и проверка на кратность 10
        /// </summary>
        /// <param name="cardKeyArray">Текущий массив цифр карты</param>
        /// <returns>Валидна карта или нет</returns>
        public bool DoStepTwo(int[] cardKeyArray)
        {
            var sum = 0;
            foreach (var digit in cardKeyArray)
                sum = sum + digit;

            return (sum % 10 == 0);
        }
    }
}
