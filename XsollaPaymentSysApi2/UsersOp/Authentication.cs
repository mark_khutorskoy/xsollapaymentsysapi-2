﻿using System;
using System.Globalization;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using XsollaPaymentSysApi2.UsersOp;

namespace XsollaPaymentSysApi2
{
    public class Authentication
    {
        public Users FindUser(string login)
        {
            Users u = null;
            using (var context = new XsollaPaymentSysContext())
            {
                foreach (var user in context.Users.Include(usr => usr.SessionInfo))
                {
                    if (user.Login == login)
                    {
                        u = user;
                        break;
                    }
                }     
            }
            return u;
        }

        public LogInInfo LogIn(string login, string pass)
        {
            Users u = FindUser(login);

            if (u == null)
            {
                return new LogInInfo(false, "Такой логин не найден");
            }

            if (!PassHash.CheckHash(u.Password, pass + u.Salt))
            {
                return new LogInInfo(false, "Неверный пароль");
            }

            using (var context = new XsollaPaymentSysContext())
            {
                SessionInfo sessionInfo = CreateSessionObj();

                //сохранили сессию и привязку в users на нее
                context.SessionInfo.Add(sessionInfo);
                context.SaveChanges();

                context.Users.Attach(u);
                u.SessionInfo = sessionInfo;
                context.SaveChanges();
            }
            return new LogInInfo(true);
        }

        public LogInInfo IsAuthorized(string login, string pass)
        {
            Users u = FindUser(login);

            if (u == null)
            {
                return new LogInInfo(false, "Такого логина нет в базе");
            }

            if (u.SessionInfo != null && !SessionExpired(u.SessionInfo.SessionExpiration))
            {
                return new LogInInfo(true);
            }

            return new LogInInfo(false, "Авторизируйтесь, пожалуйста");
        }

        /// <summary>
        /// Проверяет не занят ли логин при регистрации
        /// </summary>
        /// <param name="login"></param>
        /// <returns>true - логин не занят</returns>
        public bool CheckLoginUniqueness(string login)
        {
            using (var context = new XsollaPaymentSysContext())
            {
                return !Enumerable.Any(context.Users, user => user.Login == login);
            }
        }

        public LogInInfo SessionCheckSuccess(string sessionId)
        {
            SessionInfo curSession = null;
            using (var context = new XsollaPaymentSysContext())
            {
                foreach (var item in context.SessionInfo)
                {
                    if (item.SessionId == sessionId)
                    {
                        curSession = item;
                        break;
                    }
                }

                if (curSession != null && SessionExpired(curSession.SessionExpiration))
                {
                    context.SessionInfo.Remove(curSession);
                    context.SaveChanges();

                    return new LogInInfo(false, "Ваша сессия истекла. Начните процесс заново");
                }
            }

            return new LogInInfo(true);
        }

        public bool SessionExpired(string exptime)
        {
            return (DateTime.Now > DateTime.ParseExact(exptime, "MM/dd/yyyy HH:mm:ss",
                        CultureInfo.InvariantCulture, DateTimeStyles.None));
        }

        public SessionInfo CreateSessionObj()
        {
            const double sessionTime = 20;

            var sessionId = Guid.NewGuid();
            var expTime = DateTime.Now.AddMinutes(sessionTime);

            return new SessionInfo
            {
                SessionId = sessionId.ToString(),
                SessionExpiration = expTime.ToString(CultureInfo.InvariantCulture)
            };
        }
    }
}

