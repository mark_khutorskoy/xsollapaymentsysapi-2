﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace XsollaPaymentSysApi2
{
    public class PassHash
    {
        public static string CreateSalt(int size)
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        public static string CreateHash(string saltpassword)
        {
            using (var cr = new MD5CryptoServiceProvider())
            {
                return Convert.ToBase64String(cr.ComputeHash(Encoding.UTF8.GetBytes(saltpassword)));
            }
        }

        public static bool CheckHash(string hash, string saltpassword)
        {
            return hash == CreateHash(saltpassword);
        }
    }

}
