﻿namespace XsollaPaymentSysApi2.UsersOp
{
    public class LogInInfo
    {
        public bool Successful { get; set; }
        public string Message { get; set; }

        public LogInInfo(bool successful, string message = null)
        {
            Successful = successful;
            Message = message;
        }
    }
}
