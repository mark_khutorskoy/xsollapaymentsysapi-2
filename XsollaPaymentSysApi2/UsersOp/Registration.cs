﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XsollaPaymentSysApi2.UsersOp
{
    public class Registration
    {
        public void Registr(string login, string pass)
        {
            var salt = PassHash.CreateSalt(new Random().Next(20, 30));

            var newUser = new Users
            {
                Login = login,
                Password = PassHash.CreateHash(pass + salt),
                Salt = salt
            };

            using (var context = new XsollaPaymentSysContext())
            {
                context.Users.Add(newUser);
                context.SaveChanges();
            }

        }
    }
}
