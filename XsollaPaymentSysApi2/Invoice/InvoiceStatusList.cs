﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XsollaPaymentSysApi2
{
    public enum InvoiceStatusList
    {
        Created,
        Paid,
        Done
    }
}
