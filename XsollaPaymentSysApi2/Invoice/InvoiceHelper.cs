﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using XsollaPaymentSysApi2.Exceptions;

namespace XsollaPaymentSysApi2.Invoice
{
    public class InvoiceHelper
    {
        public int SessionAndUnpaidInvoiceSave(decimal paymentAmount, string paymentPurpose, SessionInfo sessionInfo)
        {
            var invoice = new InvoiceInfo
            {
                PaymentAmount = paymentAmount,
                PaymentPurpose = paymentPurpose,
                Status = InvoiceStatusList.Created.ToString()
            };

            using (var context = new XsollaPaymentSysContext())
            {
                context.SessionInfo.Add(sessionInfo);
                context.InvoiceInfo.Add(invoice);
                context.SaveChanges();

                return invoice.Id;
            }
        }

        public void PaidInvoiceSave(int sessionInfoId)
        {
            var newRow = new InvoiceInfo
            {
                Status = InvoiceStatusList.Paid.ToString(),
                PaymentDate = DateTime.Now
            };
            InvoiceInfo oldRow = null;

            //считываем поля записанные ранее, чтобы сформировать новую окончательную строку бд и записать ее вместо старой
            using (var context = new XsollaPaymentSysContext())
            {
                foreach (var row in context.InvoiceInfo)
                {
                    if (row.Id == sessionInfoId)
                    {
                        newRow.PaymentAmount = row.PaymentAmount;
                        newRow.PaymentPurpose = row.PaymentPurpose;
                        oldRow = row;

                        break;
                    }
                }

                if (oldRow == null)
                {
                    var message = "Первый этап заказа пропущен - сперва укажите размер платежа и назначение";
                    var exception = new InvoiceHelperException(message);
                    throw exception;
                }

                context.InvoiceInfo.Remove(oldRow);
                context.InvoiceInfo.Add(newRow);
                context.SaveChanges();
            }
        }

        public List<InvoiceInfo> GetInvoicesList(DateTime startDate, DateTime endDate)
        {
            var invlist = new List<InvoiceInfo>(); 
            using (var context = new XsollaPaymentSysContext())
            {
               invlist.AddRange(context.InvoiceInfo.Where(invoice => invoice.PaymentDate >= startDate && invoice.PaymentDate <= endDate));
            }

            return invlist;
        }
    }
}
