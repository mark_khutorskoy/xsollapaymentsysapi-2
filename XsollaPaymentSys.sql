USE [master]
GO
/****** Object:  Database [XsollaPaymentSys]    Script Date: 18.07.2020 21:48:55 ******/
CREATE DATABASE [XsollaPaymentSys]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'XsollaPaymentSys', FILENAME = N'D:\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\XsollaPaymentSys.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'XsollaPaymentSys_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\XsollaPaymentSys_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [XsollaPaymentSys] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [XsollaPaymentSys].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [XsollaPaymentSys] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET ARITHABORT OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [XsollaPaymentSys] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [XsollaPaymentSys] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET  DISABLE_BROKER 
GO
ALTER DATABASE [XsollaPaymentSys] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [XsollaPaymentSys] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET RECOVERY FULL 
GO
ALTER DATABASE [XsollaPaymentSys] SET  MULTI_USER 
GO
ALTER DATABASE [XsollaPaymentSys] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [XsollaPaymentSys] SET DB_CHAINING OFF 
GO
ALTER DATABASE [XsollaPaymentSys] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [XsollaPaymentSys] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [XsollaPaymentSys] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'XsollaPaymentSys', N'ON'
GO
USE [XsollaPaymentSys]
GO
/****** Object:  Table [dbo].[invoiceInfo]    Script Date: 18.07.2020 21:48:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[invoiceInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userId] [int] NULL,
	[paymentAmount] [decimal](18, 0) NOT NULL,
	[paymentPurpose] [nvarchar](max) NOT NULL,
	[status] [nvarchar](20) NOT NULL,
	[paymentDate] [date] NULL,
 CONSTRAINT [PK_invoiceInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sessionInfo]    Script Date: 18.07.2020 21:48:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sessionInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sessionId] [nvarchar](max) NOT NULL,
	[sessionExpiration] [nvarchar](50) NULL,
 CONSTRAINT [PK_sessionInfo_1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users]    Script Date: 18.07.2020 21:48:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[login] [nvarchar](max) NOT NULL,
	[password] [nvarchar](max) NOT NULL,
	[salt] [nvarchar](max) NOT NULL,
	[sessionInfoId] [int] NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[invoiceInfo]  WITH CHECK ADD  CONSTRAINT [FK_invoiceInfo_invoiceInfo] FOREIGN KEY([userId])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[invoiceInfo] CHECK CONSTRAINT [FK_invoiceInfo_invoiceInfo]
GO
ALTER TABLE [dbo].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_sessionInfo] FOREIGN KEY([sessionInfoId])
REFERENCES [dbo].[sessionInfo] ([id])
GO
ALTER TABLE [dbo].[users] CHECK CONSTRAINT [FK_users_sessionInfo]
GO
USE [master]
GO
ALTER DATABASE [XsollaPaymentSys] SET  READ_WRITE 
GO
